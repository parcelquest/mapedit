Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmEditMap
	Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'For the start-up form, the first instance created is the default instance.
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdTest As System.Windows.Forms.Button
	Public WithEvents txtExt As System.Windows.Forms.TextBox
	Public WithEvents cbSubfolder As System.Windows.Forms.CheckBox
	Public WithEvents txtDateLimit As System.Windows.Forms.TextBox
	Public WithEvents txtLogFile As System.Windows.Forms.TextBox
	Public WithEvents txtWidth As System.Windows.Forms.TextBox
	Public WithEvents txtHeight As System.Windows.Forms.TextBox
	Public WithEvents txtLeft As System.Windows.Forms.TextBox
	Public WithEvents txtTop As System.Windows.Forms.TextBox
	Public WithEvents ImgEdit1 As AxImgeditLibCtl.AxImgEdit
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdStart As System.Windows.Forms.Button
	Public WithEvents txtInputPath As System.Windows.Forms.TextBox
	Public WithEvents lblCounter As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblInputPath As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.components = New System.ComponentModel.Container()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditMap))
      Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
      Me.cbSubfolder = New System.Windows.Forms.CheckBox()
      Me.txtDateLimit = New System.Windows.Forms.TextBox()
      Me.cmdTest = New System.Windows.Forms.Button()
      Me.txtExt = New System.Windows.Forms.TextBox()
      Me.txtLogFile = New System.Windows.Forms.TextBox()
      Me.txtWidth = New System.Windows.Forms.TextBox()
      Me.txtHeight = New System.Windows.Forms.TextBox()
      Me.txtLeft = New System.Windows.Forms.TextBox()
      Me.txtTop = New System.Windows.Forms.TextBox()
      Me.ImgEdit1 = New AxImgeditLibCtl.AxImgEdit()
      Me.cmdExit = New System.Windows.Forms.Button()
      Me.cmdStart = New System.Windows.Forms.Button()
      Me.txtInputPath = New System.Windows.Forms.TextBox()
      Me.lblCounter = New System.Windows.Forms.Label()
      Me.Label7 = New System.Windows.Forms.Label()
      Me.Label6 = New System.Windows.Forms.Label()
      Me.Label5 = New System.Windows.Forms.Label()
      Me.Label4 = New System.Windows.Forms.Label()
      Me.Label3 = New System.Windows.Forms.Label()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.lblInputPath = New System.Windows.Forms.Label()
      CType(Me.ImgEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.SuspendLayout()
      '
      'cbSubfolder
      '
      Me.cbSubfolder.BackColor = System.Drawing.SystemColors.Control
      Me.cbSubfolder.Checked = True
      Me.cbSubfolder.CheckState = System.Windows.Forms.CheckState.Checked
      Me.cbSubfolder.Cursor = System.Windows.Forms.Cursors.Default
      Me.cbSubfolder.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cbSubfolder.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cbSubfolder.Location = New System.Drawing.Point(104, 160)
      Me.cbSubfolder.Name = "cbSubfolder"
      Me.cbSubfolder.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cbSubfolder.Size = New System.Drawing.Size(113, 17)
      Me.cbSubfolder.TabIndex = 17
      Me.cbSubfolder.Text = "Process subfolder"
      Me.ToolTip1.SetToolTip(Me.cbSubfolder, "Check this to process subfolder")
      Me.cbSubfolder.UseVisualStyleBackColor = False
      '
      'txtDateLimit
      '
      Me.txtDateLimit.AcceptsReturn = True
      Me.txtDateLimit.BackColor = System.Drawing.SystemColors.Window
      Me.txtDateLimit.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtDateLimit.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtDateLimit.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtDateLimit.Location = New System.Drawing.Point(424, 155)
      Me.txtDateLimit.MaxLength = 0
      Me.txtDateLimit.Name = "txtDateLimit"
      Me.txtDateLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtDateLimit.Size = New System.Drawing.Size(153, 22)
      Me.txtDateLimit.TabIndex = 15
      Me.txtDateLimit.Text = "<20060512"
      Me.ToolTip1.SetToolTip(Me.txtDateLimit, "Process all files created before this date")
      '
      'cmdTest
      '
      Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
      Me.cmdTest.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdTest.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdTest.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdTest.Location = New System.Drawing.Point(600, 104)
      Me.cmdTest.Name = "cmdTest"
      Me.cmdTest.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdTest.Size = New System.Drawing.Size(73, 25)
      Me.cmdTest.TabIndex = 21
      Me.cmdTest.Text = "&Test"
      Me.cmdTest.UseVisualStyleBackColor = False
      '
      'txtExt
      '
      Me.txtExt.AcceptsReturn = True
      Me.txtExt.BackColor = System.Drawing.SystemColors.Window
      Me.txtExt.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtExt.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtExt.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtExt.Location = New System.Drawing.Point(280, 155)
      Me.txtExt.MaxLength = 0
      Me.txtExt.Name = "txtExt"
      Me.txtExt.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtExt.Size = New System.Drawing.Size(49, 22)
      Me.txtExt.TabIndex = 18
      Me.txtExt.Text = "PQM"
      '
      'txtLogFile
      '
      Me.txtLogFile.AcceptsReturn = True
      Me.txtLogFile.BackColor = System.Drawing.SystemColors.Window
      Me.txtLogFile.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtLogFile.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtLogFile.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtLogFile.Location = New System.Drawing.Point(104, 104)
      Me.txtLogFile.MaxLength = 0
      Me.txtLogFile.Name = "txtLogFile"
      Me.txtLogFile.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtLogFile.Size = New System.Drawing.Size(409, 20)
      Me.txtLogFile.TabIndex = 13
      Me.txtLogFile.Text = "H:\CO_LOGS\MapMaint\MapEdit.log"
      '
      'txtWidth
      '
      Me.txtWidth.AcceptsReturn = True
      Me.txtWidth.BackColor = System.Drawing.SystemColors.Window
      Me.txtWidth.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtWidth.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtWidth.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtWidth.Location = New System.Drawing.Point(312, 64)
      Me.txtWidth.MaxLength = 0
      Me.txtWidth.Name = "txtWidth"
      Me.txtWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtWidth.Size = New System.Drawing.Size(33, 20)
      Me.txtWidth.TabIndex = 12
      Me.txtWidth.Text = "15"
      '
      'txtHeight
      '
      Me.txtHeight.AcceptsReturn = True
      Me.txtHeight.BackColor = System.Drawing.SystemColors.Window
      Me.txtHeight.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtHeight.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtHeight.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtHeight.Location = New System.Drawing.Point(424, 64)
      Me.txtHeight.MaxLength = 0
      Me.txtHeight.Name = "txtHeight"
      Me.txtHeight.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtHeight.Size = New System.Drawing.Size(33, 20)
      Me.txtHeight.TabIndex = 10
      Me.txtHeight.Text = "400"
      '
      'txtLeft
      '
      Me.txtLeft.AcceptsReturn = True
      Me.txtLeft.BackColor = System.Drawing.SystemColors.Window
      Me.txtLeft.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtLeft.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtLeft.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtLeft.Location = New System.Drawing.Point(208, 64)
      Me.txtLeft.MaxLength = 0
      Me.txtLeft.Name = "txtLeft"
      Me.txtLeft.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtLeft.Size = New System.Drawing.Size(33, 20)
      Me.txtLeft.TabIndex = 8
      Me.txtLeft.Text = "0"
      '
      'txtTop
      '
      Me.txtTop.AcceptsReturn = True
      Me.txtTop.BackColor = System.Drawing.SystemColors.Window
      Me.txtTop.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtTop.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtTop.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtTop.Location = New System.Drawing.Point(104, 64)
      Me.txtTop.MaxLength = 0
      Me.txtTop.Name = "txtTop"
      Me.txtTop.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtTop.Size = New System.Drawing.Size(33, 20)
      Me.txtTop.TabIndex = 6
      Me.txtTop.Text = "350"
      '
      'ImgEdit1
      '
      Me.ImgEdit1.Location = New System.Drawing.Point(24, 208)
      Me.ImgEdit1.Name = "ImgEdit1"
      Me.ImgEdit1.OcxState = CType(resources.GetObject("ImgEdit1.OcxState"), System.Windows.Forms.AxHost.State)
      Me.ImgEdit1.Size = New System.Drawing.Size(649, 489)
      Me.ImgEdit1.TabIndex = 4
      '
      'cmdExit
      '
      Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
      Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdExit.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdExit.Location = New System.Drawing.Point(600, 64)
      Me.cmdExit.Name = "cmdExit"
      Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdExit.Size = New System.Drawing.Size(73, 25)
      Me.cmdExit.TabIndex = 3
      Me.cmdExit.Text = "E&xit"
      Me.cmdExit.UseVisualStyleBackColor = False
      '
      'cmdStart
      '
      Me.cmdStart.BackColor = System.Drawing.SystemColors.Control
      Me.cmdStart.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdStart.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdStart.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdStart.Location = New System.Drawing.Point(600, 24)
      Me.cmdStart.Name = "cmdStart"
      Me.cmdStart.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdStart.Size = New System.Drawing.Size(73, 25)
      Me.cmdStart.TabIndex = 2
      Me.cmdStart.Text = "&Start"
      Me.cmdStart.UseVisualStyleBackColor = False
      '
      'txtInputPath
      '
      Me.txtInputPath.AcceptsReturn = True
      Me.txtInputPath.BackColor = System.Drawing.SystemColors.Window
      Me.txtInputPath.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtInputPath.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtInputPath.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtInputPath.Location = New System.Drawing.Point(104, 24)
      Me.txtInputPath.MaxLength = 0
      Me.txtInputPath.Name = "txtInputPath"
      Me.txtInputPath.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtInputPath.Size = New System.Drawing.Size(409, 20)
      Me.txtInputPath.TabIndex = 1
      Me.txtInputPath.Text = "F:\PQMaps\Lax"
      '
      'lblCounter
      '
      Me.lblCounter.BackColor = System.Drawing.SystemColors.ScrollBar
      Me.lblCounter.Cursor = System.Windows.Forms.Cursors.Default
      Me.lblCounter.ForeColor = System.Drawing.Color.Red
      Me.lblCounter.Location = New System.Drawing.Point(600, 160)
      Me.lblCounter.Name = "lblCounter"
      Me.lblCounter.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lblCounter.Size = New System.Drawing.Size(73, 17)
      Me.lblCounter.TabIndex = 20
      '
      'Label7
      '
      Me.Label7.BackColor = System.Drawing.SystemColors.Control
      Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label7.Location = New System.Drawing.Point(208, 160)
      Me.Label7.Name = "Label7"
      Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label7.Size = New System.Drawing.Size(65, 17)
      Me.Label7.TabIndex = 19
      Me.Label7.Text = "File ext."
      Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label6
      '
      Me.Label6.BackColor = System.Drawing.SystemColors.Control
      Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label6.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label6.Location = New System.Drawing.Point(335, 160)
      Me.Label6.Name = "Label6"
      Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label6.Size = New System.Drawing.Size(83, 17)
      Me.Label6.TabIndex = 16
      Me.Label6.Text = "Modified date"
      Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label5
      '
      Me.Label5.BackColor = System.Drawing.SystemColors.Control
      Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label5.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label5.Location = New System.Drawing.Point(16, 112)
      Me.Label5.Name = "Label5"
      Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label5.Size = New System.Drawing.Size(73, 17)
      Me.Label5.TabIndex = 14
      Me.Label5.Text = "Log file"
      Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label4
      '
      Me.Label4.BackColor = System.Drawing.SystemColors.Control
      Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label4.Location = New System.Drawing.Point(264, 64)
      Me.Label4.Name = "Label4"
      Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label4.Size = New System.Drawing.Size(33, 17)
      Me.Label4.TabIndex = 11
      Me.Label4.Text = "Width"
      Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label3
      '
      Me.Label3.BackColor = System.Drawing.SystemColors.Control
      Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label3.Location = New System.Drawing.Point(368, 64)
      Me.Label3.Name = "Label3"
      Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label3.Size = New System.Drawing.Size(41, 17)
      Me.Label3.TabIndex = 9
      Me.Label3.Text = "Height"
      Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label2
      '
      Me.Label2.BackColor = System.Drawing.SystemColors.Control
      Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label2.Location = New System.Drawing.Point(160, 64)
      Me.Label2.Name = "Label2"
      Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label2.Size = New System.Drawing.Size(33, 17)
      Me.Label2.TabIndex = 7
      Me.Label2.Text = "Left"
      Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label1
      '
      Me.Label1.BackColor = System.Drawing.SystemColors.Control
      Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label1.Location = New System.Drawing.Point(48, 64)
      Me.Label1.Name = "Label1"
      Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label1.Size = New System.Drawing.Size(41, 17)
      Me.Label1.TabIndex = 5
      Me.Label1.Text = "Top"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'lblInputPath
      '
      Me.lblInputPath.BackColor = System.Drawing.SystemColors.Control
      Me.lblInputPath.Cursor = System.Windows.Forms.Cursors.Default
      Me.lblInputPath.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblInputPath.ForeColor = System.Drawing.SystemColors.ControlText
      Me.lblInputPath.Location = New System.Drawing.Point(16, 32)
      Me.lblInputPath.Name = "lblInputPath"
      Me.lblInputPath.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lblInputPath.Size = New System.Drawing.Size(73, 17)
      Me.lblInputPath.TabIndex = 0
      Me.lblInputPath.Text = "Input path"
      Me.lblInputPath.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'frmEditMap
      '
      Me.AcceptButton = Me.cmdStart
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.BackColor = System.Drawing.SystemColors.Control
      Me.ClientSize = New System.Drawing.Size(696, 719)
      Me.Controls.Add(Me.cmdTest)
      Me.Controls.Add(Me.txtExt)
      Me.Controls.Add(Me.cbSubfolder)
      Me.Controls.Add(Me.txtDateLimit)
      Me.Controls.Add(Me.txtLogFile)
      Me.Controls.Add(Me.txtWidth)
      Me.Controls.Add(Me.txtHeight)
      Me.Controls.Add(Me.txtLeft)
      Me.Controls.Add(Me.txtTop)
      Me.Controls.Add(Me.ImgEdit1)
      Me.Controls.Add(Me.cmdExit)
      Me.Controls.Add(Me.cmdStart)
      Me.Controls.Add(Me.txtInputPath)
      Me.Controls.Add(Me.lblCounter)
      Me.Controls.Add(Me.Label7)
      Me.Controls.Add(Me.Label6)
      Me.Controls.Add(Me.Label5)
      Me.Controls.Add(Me.Label4)
      Me.Controls.Add(Me.Label3)
      Me.Controls.Add(Me.Label2)
      Me.Controls.Add(Me.Label1)
      Me.Controls.Add(Me.lblInputPath)
      Me.Cursor = System.Windows.Forms.Cursors.Default
      Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
      Me.Location = New System.Drawing.Point(4, 23)
      Me.Name = "frmEditMap"
      Me.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Text = "Map Editor"
      CType(Me.ImgEdit1, System.ComponentModel.ISupportInitialize).EndInit()
      Me.ResumeLayout(False)
      Me.PerformLayout()

   End Sub
#End Region 
#Region "Upgrade Support "
	Private Shared m_vb6FormDefInstance As frmEditMap
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmEditMap
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmEditMap()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Dim lCounter As Integer
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		If g_bStarted = True Then
			g_bSuspend = True
		Else
			Me.Close()
		End If
	End Sub
	
	Private Sub cmdStart_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdStart.Click
		Dim lTmp As Integer
		
		lCounter = 0
		g_lLeft = CInt(txtLeft.Text)
		g_lTop = CInt(txtTop.Text)
		g_lWidth = CInt(txtWidth.Text)
		g_lHeight = CInt(txtHeight.Text)
		g_logFile = txtLogFile.Text
		g_sExt = UCase(txtExt.Text)
		g_bSuspend = False
		g_bStarted = True
		cmdExit.Text = "Stop"
		cmdStart.Enabled = False
		
		On Error GoTo cmdStart_Error
		
		'Setup range to be to be processed
		If VB.Left(txtDateLimit.Text, 1) > "0" And VB.Left(txtDateLimit.Text, 1) < "3" Then
			g_sDateLimit = txtDateLimit.Text
		Else
			g_sDateLimit = Mid(txtDateLimit.Text, 2)
		End If
		
		If VB.Left(txtDateLimit.Text, 1) = "<" Then
			g_sFromDate = "19000101"
			lTmp = CInt(g_sDateLimit)
			lTmp = lTmp - 1
			g_sToDate = g_sDateLimit
			LogMsg("Process maps before " & g_sDateLimit)
		ElseIf VB.Left(txtDateLimit.Text, 1) = ">" Then 
			lTmp = CInt(g_sDateLimit)
			lTmp = lTmp + 1
			g_sFromDate = g_sDateLimit
			g_sToDate = VB6.Format(Now, "yyyymmdd")
			lTmp = CInt(g_sToDate)
			lTmp = lTmp + 1
			g_sToDate = CStr(lTmp)
			LogMsg("Process maps after " & g_sDateLimit)
		ElseIf VB.Left(txtDateLimit.Text, 1) = "=" Then 
			g_sFromDate = g_sDateLimit
			g_sToDate = g_sDateLimit
			LogMsg("Process maps on " & g_sDateLimit)
		Else
			If Len(g_sDateLimit) <> 17 Then
				MsgBox("Invalid date range.  Please correct and retry", MsgBoxStyle.OKOnly, "Invalid entry")
				Exit Sub
			Else
				g_sFromDate = VB.Left(g_sDateLimit, 8)
				g_sToDate = VB.Right(g_sDateLimit, 8)
				LogMsg("Process maps between " & g_sFromDate & " and " & g_sToDate)
			End If
		End If
		
		If cbSubfolder.CheckState = System.Windows.Forms.CheckState.Checked Then
			g_bSubdir = True
		Else
			g_bSubdir = False
		End If
		
		
		On Error Resume Next
		Call ProcessFiles((txtInputPath.Text))
		GoTo cmdStart_Exit
		
cmdStart_Error: 
		MsgBox("Error: " & Err.Description)
		
cmdStart_Exit: 
		cmdExit.Text = "E&xit"
		cmdStart.Enabled = True
		g_bSuspend = False
		g_bStarted = False
	End Sub
	
	Public Function ProcessImage(ByRef sImageName As String) As Short
		Dim iStatus As Short
		Dim lTmp, lIdx As Integer
		On Error GoTo ProcessImage_Error
		
		iStatus = 3
		ImgEdit1.Image = sImageName
		ImgEdit1.FitTo((Routines.FitTo.ftFitToWidth))
		ImgEdit1.Page = 1
		ImgEdit1.DisplayScaleAlgorithm = 2
		ImgEdit1.Display()
		If ImgEdit1.StatusCode <> 0 Then
			LogMsg(Err.Description & " (error code " & Hex(ImgEdit1.StatusCode) & ")")
			Exit Function
		End If
		
		System.Windows.Forms.Application.DoEvents()
		For lIdx = 1 To ImgEdit1.PageCount
			iStatus = 2
			ImgEdit1.Page = lIdx
			ImgEdit1.Display()
			ImgEdit1.DeleteImageData(g_lLeft, g_lTop, g_lWidth, g_lHeight)
			iStatus = 1
			ImgEdit1.Save()
		Next 
		
		iStatus = 0
		lCounter = lCounter + 1
		lblCounter.Text = CStr(lCounter)
		
ProcessImage_Error: 
		ProcessImage = iStatus
	End Function
	
	Private Sub cmdTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTest.Click
		Dim sTmp As String
		Dim bRet As Boolean
		
		g_lTop = CInt(txtTop.Text)
		g_lLeft = CInt(txtLeft.Text)
		g_lHeight = CInt(txtHeight.Text)
		g_lWidth = CInt(txtWidth.Text)
		g_logFile = txtLogFile.Text
		g_sExt = UCase(txtExt.Text)
		sTmp = txtInputPath.Text
		bRet = ProcessImage(sTmp)
		If bRet = False Then
			MsgBox("Error processing " & sTmp)
		End If
	End Sub
	
	Private Sub frmEditMap_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If g_lWidth > 0 Then
			txtLeft.Text = CStr(g_lLeft)
			txtTop.Text = CStr(g_lTop)
			txtWidth.Text = CStr(g_lWidth)
			txtHeight.Text = CStr(g_lHeight)
		End If
		
		txtLogFile.Text = g_logFile
		txtInputPath.Text = g_sInputPath
		txtExt.Text = g_sExt
		txtDateLimit.Text = g_sDateLimit
		
      Me.Text = "MapEdit "
   End Sub

 End Class