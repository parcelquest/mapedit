Option Strict Off
Option Explicit On
Module logFile
	Public g_logFile As String
	
	Public Sub LogMsg(ByVal strMsg As String)
		Dim ff As Short
		
		On Error GoTo ErrorHandler
		ff = FreeFile
		
		FileOpen(ff, g_logFile, OpenMode.Append)
		PrintLine(ff, Now & vbTab & strMsg)
		FileClose(ff)
		Exit Sub
		
ErrorHandler: 
		'MsgBox "Please check for diskspace on " & g_logFile
	End Sub
	
	Public Sub LogMsg0(ByVal strMsg As String)
		Dim ff As Short
		
		On Error GoTo ErrorHandler
		ff = FreeFile
		
		FileOpen(ff, g_logFile, OpenMode.Append)
		PrintLine(ff, strMsg)
		FileClose(ff)
		
ErrorHandler: 
	End Sub
End Module