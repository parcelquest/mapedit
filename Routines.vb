Option Strict Off
Option Explicit On
Module Routines
	'**************************************************************
	' Global (Public) Variables
	'**************************************************************
	
	'----------------------------------------------------------
	' Generic Return Codes
	'----------------------------------------------------------
	Public gblnInitialDisplayOfThisImage As Boolean
	
	'----------------------------------------------------------
	' OS, directory locations:  set in frmMain.Form_Load.
	'----------------------------------------------------------
	Public gintOS As Short
	Public gstrAppDirectory As String
	Public gstrFlowDirectory As String
	Public gstrWindowsDirectory As String
	Public gstrWindowsTempDirectory As String
	
	'----------------------------------------------------------
	' Directory locations:  set in option boxes.
	'----------------------------------------------------------
	Public gstrDefaultDirectory As String 'frmGenOpt option
	
	'----------------------------------------------------------
	' User interface information.
	'----------------------------------------------------------
	Public gblnInitialImageHasBeenShown As Boolean
	Public gblnThisPageAlreadyDisplayed As Boolean
	
	'----------------------------------------------------------
	' File information.
	'----------------------------------------------------------
	Public gblnFileNew As Boolean 'True = File, New selected from the menu.
	Public gblnReadOnly As Boolean 'True = File is read only.
	Public gblnTempFile As Boolean 'True = File is a temp file.
	Public gintFileType As Short 'TIFF, AWD...
	Public gintFileLocation As Short 'Local, Web, 1.x, 3.x server.
	
	'----------------------------------------------------------
	' Magnifier status.
	'----------------------------------------------------------
	Public gblnMagnifierShown As Boolean
	
	'----------------------------------------------------------
	' Show Toolbar preferences:  set within frmToolbars.
	'----------------------------------------------------------
	Public gblnShowStandardToolbar As Boolean
	Public gblnShowImagingToolbar As Boolean
	Public gblnShowAnnotationToolbar As Boolean
	Public gblnShowScanningToolbar As Boolean
	Public gblnShowWebToolbar As Boolean
	Public gblnShowToolbar1 As Boolean
	Public gblnShowToolbar2 As Boolean
	Public gblnShowToolbar3 As Boolean
	
	'----------------------------------------------------------
	' Toolbar locations (Top/Bottom):  set by dragging toolbars.
	'----------------------------------------------------------
	Public gstrToolbar1Location As String
	Public gstrToolbar2Location As String
	Public gstrToolbar3Location As String
	
	'----------------------------------------------------------
	' User preferences.
	'----------------------------------------------------------
	Public gblnIncludeAnnotations As Boolean
	Public gblnOpenContactSheet As Boolean
	Public gblnShowScrollbars As Boolean
	Public gstrZoomPreference As String
	
	'----------------------------------------------------------
	' Variables used when inserting/appending/moving images.
	'----------------------------------------------------------
	Public gstrInsertAppend As String
	Public gstrFullPathSourceFileName As String
	Public gstrSourceFileName As String
	Public glngNumPages As Integer
	Public glngSourcePage As Integer
	Public glngMoveBeforePage As Integer
	
	Public gdblPaperWidth As Double
	Public gdblPaperHeight As Double
	
	'**************************************************************
	' Global (Public) Constants
	'**************************************************************
	
	'----------------------------------------------------------
	' Annotation types.
	'----------------------------------------------------------
	Public Const NO_TOOL As Short = 0
	Public Const ANNO_SELECTION As Short = 1
	Public Const ANNO_FREEHAND_LINE As Short = 2
	Public Const ANNO_HIGHLIGHTER As Short = 3
	Public Const ANNO_STRAIGHT_LINE As Short = 4
	Public Const ANNO_HOLLOW_RECTANGLE As Short = 5
	Public Const ANNO_FILLED_RECTANGLE As Short = 6
	Public Const ANNO_TYPED_TEXT As Short = 7
	Public Const ANNO_ATTACH_A_NOTE As Short = 8
	Public Const ANNO_TEXT_FROM_FILE As Short = 9
	Public Const ANNO_RUBBER_STAMP As Short = 10
	Public Const ANNO_HYPERLINK As Short = 11
	Public Const ANNO_OCR_ZONES As Short = 12
	
	'----------------------------------------------------------
	' PageType constants.
	'----------------------------------------------------------
	Public Const BLACK_AND_WHITE_PAGE As Short = 1
	Public Const GRAY4_PAGE As Short = 2
	Public Const GRAY8_PAGE As Short = 3
	Public Const PALETTIZED4_PAGE As Short = 4
	Public Const PALETTIZED8_PAGE As Short = 5
	Public Const RGB24_PAGE As Short = 6
	Public Const BGR24_PAGE As Short = 7
	
	'----------------------------------------------------------
	' "Color" constants.
	'----------------------------------------------------------
	Public Enum Color
		clBlackAndWhite = 0
		cl16ShadesOfGray = 1
		cl256ShadesOfGray = 2
		cl256Colors = 3
		clTrueColor = 4
	End Enum
	
	'----------------------------------------------------------
	' "Document Type" constants.
	'----------------------------------------------------------
	Public Enum DocumentType
		dtTextOnly = 0
		dtArticle = 1
		dtColorArticle = 2
		dtPhotograph = 3
		dtBusinessCard = 4
		dtLineDrawing = 5
		dtPoorQuality = 6
		dtLegalDocument = 7
		dtCustom = 8
	End Enum
	
	'----------------------------------------------------------
	' "File Location" constants.
	'----------------------------------------------------------
	Public Enum FileLocation
		flLocal = 0
		flWeb = 1
		fl1xServer = 2
		fl3xServer = 3
	End Enum
	
	'----------------------------------------------------------
	' "File Type" constants.
	'----------------------------------------------------------
	Public Enum FileType
		ftUnknown = 0
		ftTIFF = 1
		ftAWD = 2
		ftBMP = 3
		ftPCX = 4
		ftDCX = 5
		ftJPEG = 6
		ftXIF = 7
		ftGIF = 8
		ftWIFF = 9
	End Enum
	
	'----------------------------------------------------------
	' "Fit To" constants.
	'----------------------------------------------------------
	Public Enum FitTo
		ftBestFit = 0
		ftFitToWidth = 1
		ftFitToHeight = 2
		ftInchToInch = 3
	End Enum
	
	'----------------------------------------------------------
	' "Operating System" constants.
	'----------------------------------------------------------
	Public Enum OperatingSystem
		osWindows31 = 0
		osWindows9x = 1
		osWindowsNT = 2
	End Enum
	
	'----------------------------------------------------------
	' "Paper Size" constants.
	'----------------------------------------------------------
	Public Enum PaperSize
		psLetter = 0
		psTabloid = 1
		psLedger = 2
		psLegal = 3
		psStatement = 4
		psExecutive = 5
		psA3 = 6
		psA4 = 7
		psA5 = 8
		psB4ISO = 9
		psB4JIS = 10
		psB5ISO = 11
		psB5JIS = 12
		psFolio = 13
		psQuarto = 14
		ps10x14 = 15
		psCustom = 16
	End Enum
	
	'----------------------------------------------------------
	' Message Box constants.
	'----------------------------------------------------------
	Public Const IMAGE_CHANGED As String = "Do you want to save the changes to "
	Public Const CLIPBOARD_IMAGE_TOO_LARGE As String = "The image in the clipboard is larger than the image.  Would you like the image enlarged?"
	Public Const ANNO_PERMANENT As String = "Saving changes will make all annotations permanent."
	Public Const SAVE_BEFORE_PRINT As String = "The image must be saved first if changes are to be printed.  Do you want to save the image?"
	Public Const UNKNOWN_FILE_1 As String = "The document to be opened, "
	Public Const UNKNOWN_FILE_2 As String = ", does not exist or cannot be found."
	Public Const MAX_ZOOM As String = "At maximum zoom ("
	Public Const MIN_ZOOM As String = "At minimum zoom ("
	Public Const ZOOM_2 As String = "%)."
	Public Const FROM_PAGE_NUMERIC As String = "The From page number must be numeric."
	Public Const TO_PAGE_NUMERIC As String = "The To page number must be numeric."
	Public Const PAGE_NUMBER_FROM_1 As String = "Enter a page number between 1 and "
	Public Const VALID_FROM_TO As String = "The 'From' value cannot be greater than the 'To' value."
	Public Const INVALID_LOCATION As String = " is not a valid default location.  The default location must be the path to an existing folder on your computer."
	Public Const ZOOM_FACTOR_BETWEEN As String = "Enter a zoom factor between "
	
	'----------------------------------------------------------
	' Registry constants.
	'----------------------------------------------------------
	Public Const APP_NAME As String = "ImgEditor"
	Public Const CONTACT_PREF_KEY As String = "Contact Sheet Preferences"
	Public Const GENERAL_PREF_KEY As String = "General Preferences"
	Public Const SERVER_PREF_KEY As String = "Image Server Preferences"
	Public Const RECENT_FILE_KEY As String = "Recent Files"
	Public Const TOOLBAR_PREF_KEY As String = "Toolbar Preferences"
	Public Const USER_PREF_KEY As String = "User Preferences"
	Public Const WINDOW_POS_KEY As String = "Window Position"
	
	'----------------------------------------------------------
	' Screen format constants.
	'----------------------------------------------------------
	Public Enum CursorFormat
		cfDrag = 0
		cfSelectImage = 1
		cfSelectAnnoZones = 2
	End Enum
	
	Public Enum ViewMode
		vmOnePage = 0
		vmThumbnails = 1
		vmPageAndThumbnails = 2
	End Enum
	
	Public Enum WindowFormat
		wfDividerMidWidth = 26
		wfDividerWidth = 52
		wfWindowWidth = 40
	End Enum
	
	'----------------------------------------------------------
	' Zoom constants:  Used as limits for zooming the image.
	'----------------------------------------------------------
	Public Enum ZoomLimits
		zlZoomMinimum = 2
		zlZoomMaximum = 6500
	End Enum
	
	'----------------------------------------------------------
	' Miscellaneous constants.
	'----------------------------------------------------------
	Public Const CANCEL_PRESSED As Short = 32755
	Public Const FILENAME_LENGTH_MAX As Short = 30
	
	Public Function CleanString(ByRef vntString As Object, ByRef strSearch As String) As String
		'**************************************************************
		' PUBLIC FUNCTION CleanString:  Removes a string of characters
		' (strSearch) from a string (vntString).
		'**************************************************************
		'** Integers
		Dim intPosition As Short
		'** Strings
		Dim strWorkingString As String
		
		'UPGRADE_WARNING: Couldn't resolve default property of object vntString. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		strWorkingString = vntString
		Do While InStr(1, strWorkingString, strSearch)
			intPosition = InStr(1, strWorkingString, strSearch)
			strWorkingString = Left(strWorkingString, intPosition - 1) & Mid(strWorkingString, intPosition + Len(strSearch))
		Loop 
		CleanString = strWorkingString
		
	End Function
	
	Public Function StripPath(ByRef strFullPathFileName As String) As String
		'**************************************************************
		' PUBLIC FUNCTION StripPath:  This strips off a file path to
		' get the file name.  This is called from GetSaveRecentFiles,
		' PerformFileOpen, UpdateRecentFiles, mnuPageAppendItem,
		' and mnuPageInsertItem.
		'**************************************************************
		'** Integers
		Dim intBackslashPosition As Short
		'** Strings
		Dim strWorkingString As String
		
		'----------------------------------------------------------
		' Look for a backslash.  If you find one, strip off
		' everything to the left of the backslash.  Do this until
		' there are no more backslashes.
		'----------------------------------------------------------
		strWorkingString = strFullPathFileName
		Do 
			intBackslashPosition = InStr(1, strWorkingString, "\")
			If intBackslashPosition > 0 Then
				strWorkingString = Mid(strWorkingString, intBackslashPosition + 1)
			End If
		Loop Until intBackslashPosition = 0
		StripPath = Trim(strWorkingString)
		
	End Function
End Module