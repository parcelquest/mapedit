Option Strict Off
Option Explicit On
Module WinAPIs
	
	'**************************************************************
	' Windows APIs
	'**************************************************************
	
	'----------------------------------------------------------
	' Windows API declares.
	'----------------------------------------------------------
	Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Integer, ByVal nIndex As Integer) As Integer
	
	Public Declare Function GetSystemDirectory Lib "kernel32"  Alias "GetSystemDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	'UPGRADE_WARNING: Structure OSVERSIONINFO may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
	Public Declare Function GetVersionExInt Lib "kernel32"  Alias "GetVersionExA"(ByRef lpVersionInformation As OSVERSIONINFO) As Short
	
	Public Declare Function GetWindowsDirectory Lib "kernel32"  Alias "GetWindowsDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	Public Declare Function RegOpenKeyEx Lib "advapi32.dll"  Alias "RegOpenKeyExA"(ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer
	
   Public Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As String, ByRef lpcbData As Integer) As Integer ' Note that if you declare the lpData parameter as String, you must pass it By Value.
	
	Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Integer) As Integer
	
	Public Declare Function WinExec Lib "kernel32" (ByVal lpCmdLine As String, ByVal nCmdShow As Integer) As Integer
	
	'----------------------------------------------------------
	' Windows API data structures.
	'----------------------------------------------------------
	Structure OSVERSIONINFO
		Dim dwOSVersionInfoSize As Integer
		Dim dwMajorVersion As Integer
		Dim dwMinorVersion As Integer
		Dim dwBuildNumber As Integer
		Dim dwPlatformId As Integer
		<VBFixedString(128),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=128)> Public szCSDVersion As String
	End Structure
	
	'----------------------------------------------------------
	' Windows API constants.
	'----------------------------------------------------------
	Public Const VER_PLATFORM_WIN32s As Short = 0
	Public Const VER_PLATFORM_WIN32_WINDOWS As Short = 1
	Public Const VER_PLATFORM_WIN32_NT As Short = 2
	
	Public Const HKEY_CLASSES_ROOT As Integer = &H80000000
	Public Const HKEY_CURRENT_USER As Integer = &H80000001
	Public Const HKEY_LOCAL_MACHINE As Integer = &H80000002
	Public Const HKEY_USERS As Integer = &H80000003
	Public Const HKEY_PERFORMANCE_DATA As Integer = &H80000004
	Public Const HKEY_CURRENT_CONFIG As Integer = &H80000005
	Public Const HKEY_DYN_DATA As Integer = &H80000006
	
	Public Const REG_SZ As Integer = 1
	Public Const REG_BINARY As Integer = 3
	Public Const REG_DWORD As Integer = 4
	
	Public Const KEY_QUERY_VALUE As Short = &H1s
	Public Const KEY_ENUMERATE_SUB_KEYS As Short = &H8s
	Public Const KEY_NOTIFY As Short = &H10s
	Public Const READ_CONTROL As Integer = &H20000
	Public Const STANDARD_RIGHTS_READ As Integer = (READ_CONTROL)
	Public Const SYNCHRONIZE As Integer = &H100000
	Public Const KEY_READ As Boolean = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
	
	Public Const ERROR_SUCCESS As Short = 0
	Public Const ERROR_BADDB As Short = 1
	Public Const ERROR_BADKEY As Short = 2
	Public Const ERROR_CANTOPEN As Short = 3
	Public Const ERROR_CANTREAD As Short = 4
	Public Const ERROR_CANTWRITE As Short = 5
	Public Const ERROR_OUTOFMEMORY As Short = 6
	Public Const ERROR_INVALID_PARAMETER As Short = 7
	Public Const ERROR_ACCESS_DENIED As Short = 8
	Public Const ERROR_INVALID_PARAMETERS As Short = 87
	Public Const ERROR_NO_MORE_ITEMS As Short = 259
	
	Public Function GetFlowDirectory(ByRef WithSlash As Boolean) As String
		'**************************************************************
		' PUBLIC FUNCTION GetFlowDirectory:  Returns the path to the
		' directory containing FLOW.EXE, with or without a trailing
		' backslash.
		'**************************************************************
		'** Integers, Longs
		Dim intPosition As Short
		Dim hKey As Integer
		Dim lngDataLength As Integer
		Dim lngResult As Integer
		'** Strings
		Dim strData As String
		Dim strWorkingString As String
		
		'----------------------------------------------------------
		' Find where FLOW.EXE is installed from the Registry.
		'----------------------------------------------------------
		lngDataLength = 255
		strData = Space(lngDataLength)
		lngResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\App Paths\FLOW.EXE", 0, KEY_QUERY_VALUE, hKey)
		If lngResult = ERROR_SUCCESS Then
			lngResult = RegQueryValueEx(hKey, "Path", 0, REG_SZ, strData, lngDataLength)
			If lngResult = ERROR_SUCCESS Then
				If Len(strData) > 0 Then
					strWorkingString = strData
					intPosition = InStr(strWorkingString, Chr(0))
					If intPosition > 0 Then
						strWorkingString = Left(strWorkingString, intPosition - 1)
					End If
				Else
					strWorkingString = ""
				End If
			Else
				strWorkingString = ""
			End If
		Else
			strWorkingString = ""
		End If
		
		Call RegCloseKey(hKey)
		
		'----------------------------------------------------------
		' Add or Remove the slash depending on what was returned,
		' and the value of WithSlash.
		'----------------------------------------------------------
		If Right(strWorkingString, 1) <> "\" And WithSlash Then
			GetFlowDirectory = strWorkingString & "\"
		ElseIf Right(strWorkingString, 1) = "\" And Not WithSlash Then 
			GetFlowDirectory = Left(strWorkingString, Len(strWorkingString) - 1)
		Else
			GetFlowDirectory = strWorkingString
		End If
		
	End Function
	
	Public Function GetWindowsDir(ByRef WithSlash As Boolean) As String
		'**************************************************************
		' PUBLIC FUNCTION GetWindowsDir:  Returns the path to the Windows
		' directory with or without a trailing backslash.
		'**************************************************************
		'** Longs
		Dim lngResult As Integer
		'** Strings
		Dim lpBuffer As String
		Dim strGetWin As String
		
		'----------------------------------------------------------
		' Initalize a buffer that is large enough to hold the
		' result, otherwise you'll get a GPF.
		'----------------------------------------------------------
		lpBuffer = Space(2048)
		
		'----------------------------------------------------------
		' Call the function, and strip the null terminator using
		' the return value.
		'----------------------------------------------------------
		lngResult = GetWindowsDirectory(lpBuffer, Len(lpBuffer))
		strGetWin = LCase(Left(lpBuffer, lngResult))
		
		'----------------------------------------------------------
		' Add or Remove the slash depending on what was returned,
		' and the value of WithSlash.
		'----------------------------------------------------------
		If Right(strGetWin, 1) <> "\" And WithSlash Then
			GetWindowsDir = strGetWin & "\"
		ElseIf Right(strGetWin, 1) = "\" And Not WithSlash Then 
			GetWindowsDir = Left(strGetWin, Len(strGetWin) - 1)
		Else
			GetWindowsDir = strGetWin
		End If
		
	End Function
End Module