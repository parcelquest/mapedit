Option Strict Off
Option Explicit On
Module modMain
	
	'Global other variables
	Public g_bSubdir As Boolean 'Subdirectory indicator
	Public g_sDateLimit As String 'Process files based on file created date
	Public g_iDateRange As Short '-1: before, 0: same date, 1: after specified date
	Public g_sFromDate As String
	Public g_sToDate As String
	Public g_sInputPath As String
	Public g_sExt As String
	Public g_lTop As Integer
	Public g_lLeft As Integer
	Public g_lHeight As Integer
	Public g_lWidth As Integer
	Public g_bSuspend As Boolean
	Public g_bStarted As Boolean
	
	Public Sub ProcessFiles(ByRef sDir As String)
		Dim sFile, sDate As String
		Dim myDate As Date
		
		'Add a trailing slash if necessary
		If Right(sDir, 1) <> "\" Then
			sDir = sDir & "\"
		End If
		
		On Error GoTo ProcessFiles_Error
		
		'Do subdirectories only if checked...
		Dim fso As New Scripting.FileSystemObject
		Dim f, sf As Scripting.Folder
		Dim fn As Scripting.File
		
		'Get a reference to the Folder object.
		f = fso.GetFolder(sDir)
		
		'Process files in the root first, then subfolders
		If f.Files.Count > 0 Then
			LogMsg("Processing root: " & sDir)
			
			For	Each fn In f.Files
				If g_bSuspend = True Then
					LogMsg("Exit due to stop")
					Exit For
				End If
				
				myDate = fn.DateLastModified
            sDate = Format(myDate, "yyyyMMdd")
				If (sDate >= g_sFromDate) And (sDate <= g_sToDate) Then
					If UCase(Right(fn.Name, 3)) = g_sExt Or g_sExt = "*" Then
						sFile = sDir & fn.Name
						Call ProcessFile(sFile)
					End If
				End If
			Next fn
		End If
		
		'Iterate through subfolders.
		If g_bSubdir = True Then
			For	Each sf In f.SubFolders
				If g_bSuspend = True Then
					Exit For
				End If
				
				LogMsg("Processing subfolder: " & sDir & sf.Name)
				For	Each fn In sf.Files
					If g_bSuspend = True Then
						Exit For
					End If
					
					myDate = fn.DateLastModified
               sDate = Format(myDate, "yyyyMMdd")
					If (sDate >= g_sFromDate) And (sDate <= g_sToDate) Then
						If UCase(Right(fn.Name, 3)) = g_sExt Or g_sExt = "*" Then
							sFile = sDir & sf.Name & "\" & fn.Name
							Call ProcessFile(sFile)
						End If
					End If
				Next fn
			Next sf
		End If
		
		Exit Sub
		
ProcessFiles_Error: 
		MsgBox("Error in ProcessFiles().  Err=" & Err.Description)
	End Sub
	
	Private Sub ProcessFile(ByRef sFile As String)
		Dim iRet As Short
		Dim sTmp As String
		
		LogMsg0(sFile)
		'Call the conversion routine...
		iRet = frmEditMap.DefInstance.ProcessImage(sFile)
		
		If iRet > 0 Then
			Select Case iRet
				Case 1
					sTmp = "displaying image"
				Case 2
					sTmp = "displaying next page in image"
				Case 3
					sTmp = "saving image"
				Case Else
					sTmp = "unknown"
			End Select
			
			LogMsg("***** Error " & sTmp)
		End If
	End Sub
	
   Public Sub Main()
      g_lTop = GetIniDataInt(VB6.GetEXEName(), "ClearArea", "Top")
      g_lLeft = GetIniDataInt(VB6.GetEXEName(), "ClearArea", "Left")
      g_lHeight = GetIniDataInt(VB6.GetEXEName(), "ClearArea", "Height")
      g_lWidth = GetIniDataInt(VB6.GetEXEName(), "ClearArea", "Width")

      g_logFile = GetIniData(VB6.GetEXEName(), "System", "Logfile")
      g_sInputPath = GetIniData(VB6.GetEXEName(), "System", "InputPath")

      g_sExt = GetIniData(VB6.GetEXEName(), "Filter", "MapExt")
      g_sDateLimit = GetIniData(VB6.GetEXEName(), "Filter", "FileDate")

      g_bSuspend = False

      frmEditMap.DefInstance.ShowDialog()

      LogMsg("Exit MapEdit ...")
   End Sub
End Module