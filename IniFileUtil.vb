Option Strict Off
Option Explicit On
Module IniFileUtil
	
   Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
   Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
   Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lDefault As Short, ByVal lpFileName As String) As Integer
	Declare Function GetWindowsDirectory Lib "kernel32"  Alias "GetWindowsDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	Function GetIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As String
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim lpFileName As String
		Dim Length As Short
		
		lpDefault = ""
		lpReturnString = Space(256)
		size = Len(lpReturnString)
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		Length = GetPrivateProfileString(lpSectionName, lpKeyName, lpDefault, lpReturnString, size, lpFileName)
		GetIniData = Left(lpReturnString, Length)
	End Function
	
	Function GetIniDataInt(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As Short
		Dim lpFileName As String
		
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		GetIniDataInt = GetPrivateProfileInt(lpSectionName, lpKeyName, 0, lpFileName)
	End Function
	
	Function SaveIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef lpValue As String) As Boolean
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim lpFileName As String
		Dim Length As Short
		
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		SaveIniData = WritePrivateProfileString(lpSectionName, lpKeyName, lpValue, lpFileName)
	End Function
	
	Function GetIniDataEx(ByRef lpFileName As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As String
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim Length As Short
		
		lpDefault = ""
		lpReturnString = Space(256)
		Length = GetPrivateProfileString(lpSectionName, lpKeyName, lpDefault, lpReturnString, 255, lpFileName)
		GetIniDataEx = Left(lpReturnString, Length)
	End Function
	
	Function SaveIniDataEx(ByRef lpFileName As String, ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef lpValue As String) As Boolean
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim Length As Short
		
		SaveIniDataEx = WritePrivateProfileString(lpSectionName, lpKeyName, lpValue, lpFileName)
	End Function
	
	Function GetPrivateIniString(ByRef sIniFile As String, ByRef sSection As String, ByRef sKey As String) As String
		Dim sDefault As String
		Dim sReturnString As String
		Dim Length As Short
		
		sDefault = ""
		sReturnString = Space(256)
		Length = GetPrivateProfileString(sSection, sKey, sDefault, sReturnString, 255, sIniFile)
		GetPrivateIniString = Left(sReturnString, Length)
	End Function
	
	Function GetPrivateIniInt(ByRef sIniFile As String, ByRef sSection As String, ByRef sKey As String) As Integer
		Dim lRet As Integer
		
		lRet = GetPrivateProfileInt(sSection, sKey, 0, sIniFile)
		GetPrivateIniInt = lRet
	End Function
	
	Function GetPrivateIniBool(ByRef sIniFile As String, ByRef sSection As String, ByRef sKey As String) As Boolean
		Dim sRet As String
		Dim bRet As Boolean
		
		sRet = GetPrivateIniString(sIniFile, sSection, sKey)
		If sRet = "ON" Or sRet = "True" Or sRet = "Yes" Then
			bRet = True
		Else
			bRet = False
		End If
		
		GetPrivateIniBool = bRet
	End Function
End Module